# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{%- set formula_path="nginx" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as nginx with context -%}

include:
  - nginx

{{ formula.namespace }}.web-config:
  file.managed:
    - name: {{ nginx.configdir.path | path_join("poudriere.conf") }}
    - source: {{ formula.url ~ "files/nginx.conf" }}
    - template: jinja
    - user: {{ nginx.configfile.user | yaml_encode }}
    - group: {{ nginx.configfile.group | yaml_encode }}
    - mode: {{ nginx.configfile.mode | yaml_encode }}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - file: {{ nginx.namespace }}.configdir
    - watch_in:
        service: {{ nginx.namespace }}.start

#!/bin/sh
set -e

usage() {
    echo "Usage: async-task.sh -t <task_token> -p <program_name> command"
    exit 1
}

# Parse arguments
while getopts t:p:s: opt
do
  case "$opt" in
    t) TASK_TOKEN=$OPTARG;;
    p) PROGRAM_NAME=$OPTARG;;
    \?) usage
  esac
done

# Capture everything everything after the arguments
shift $((OPTIND - 1))
COMMAND=$*

# Validate input
[ "$TASK_TOKEN" = "" ] && usage
[ "$PROGRAM_NAME" = "" ] && usage
[ "$COMMAND" = "" ] && usage

# Define logging locations
LOG_DIR=/var/log/portbuilder
LOG_GROUP=/portbuilder/${PROGRAM_NAME}
LOG_FILE=${LOG_DIR}/${PROGRAM_NAME}
LOG_STREAM=$(date "+%Y/%m/%d")/${AWS_SSM_INSTANCE_ID}
mkdir -p ${LOG_DIR}

# Run the command, callback to the Step Function, and log to CloudWatch
aws-async-task ${TASK_TOKEN} ${COMMAND} 2>&1 | cloudwatch_logger -r ${AWS_SSM_REGION_NAME} -g ${LOG_GROUP} -s ${LOG_STREAM} >> ${LOG_FILE} 2>&1 &

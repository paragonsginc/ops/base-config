#!/bin/sh

build() {
    RELEASE=$1
    PORTS=$2
    SETNAME=$3
    CPUARCH=amd64

    # Parse the release number
    RELMAJ=$(echo ${RELEASE} | cut -f1 -d.)
    RELMIN=$(echo ${RELEASE} | cut -f2 -d.)
    JAILNAME=freebsd-${RELMAJ}-${RELMIN}-${CPUARCH}
    PKGSET=${JAILNAME}-${PORTS}-${SETNAME}

    # Build ports
    if /usr/local/portbuilder/buildports.sh -p ${PORTS} -z ${SETNAME} -r ${RELEASE} -f "/usr/local/poudriere/config/${SETNAME}.pkglist"
    then
        # Publish packages to S3
        /usr/local/portbuilder/publish.sh -p ${PKGSET} -b repo.paragonsginc.com -d "freebsd/FreeBSD:${RELMAJ}:amd64/${PORTS}/${SETNAME}"
    fi
}

purge() {
    RELEASE=$1
    PORTS=$2
    SETNAME=$3
    CPUARCH=amd64

    # Parse the release number
    RELMAJ=$(echo ${RELEASE} | cut -f1 -d.)
    RELMIN=$(echo ${RELEASE} | cut -f2 -d.)
    JAILNAME=freebsd-${RELMAJ}-${RELMIN}-${CPUARCH}
    PKGSET=${JAILNAME}-${PORTS}-${SETNAME}

    # Delete log files
    poudriere logclean -z ${SETNAME} -j ${JAILNAME} -p ${PORTS} -a -y

    # Delete cache files
    rm -rfx "/usr/local/poudriere/data/cache/${PKGSET}"

    # Delete package files
    rm -rfx "/usr/local/poudriere/data/packages/${PKGSET}"
}

# Run portshaker to update ports tree
if portshaker -v
then
    # Build current configurations
    build 13.1 daily freeipa
    build 13.1 quarterly freeipa
    build 12.3 daily freeipa
    build 12.3 quarterly freeipa

    # Purge obsolete configurations
    purge 13.0 daily freeipa
    purge 13.0 quarterly freeipa
    purge 12.2 daily freeipa
    purge 12.2 quarterly freeipa
fi

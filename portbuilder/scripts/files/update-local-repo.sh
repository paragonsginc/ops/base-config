#!/bin/sh

QUARTERLY_BRANCH=$(/usr/local/bin/git ls-remote --heads https://github.com/freebsd/freebsd-ports.git '20??Q*' | tail -1 | cut -d '/' -f 3)
DAILY_BRANCH=main

cd /mnt/ports/freebsd-ports.git

git branch -D quarterly
git fetch upstream ${QUARTERLY_BRANCH}:quarterly || exit 1

git branch -D daily
git fetch upstream ${DAILY_BRANCH}:daily || exit 1

echo Source tree update completed successfully
exit 0

#!/bin/sh

usage() {
  echo "USAGE: fetch-shared.sh -p <pkgset> -s <shared_folder>"
  echo "  pkgset: The unique name of the poudriere package bundle (ex: freebsd-12-2-amd64-daily-freeipa)"
  echo "  shared: The name shared folder containing the Poudriere 'data' folder"
  exit 1
}

sync() {
  SOURCE=$1
  DEST=$2

  mkdir -p "${SOURCE}"
  mkdir -p "${DEST}"
  echo "Synchronizing from ${SOURCE} to ${DEST}"
  rsync -aHAXEx "${SOURCE}/" "${DEST}"
}

# Parse arguments
while getopts p:s: opt
do
  case "$opt" in
    p) pkgset=$OPTARG;;
    s) shared=$OPTARG;;
    \?) usage
  esac
done

# Validate arguments
shared=$(readlink -f "${shared}")
test -n "${pkgset}" || usage
test -n "${shared}" || usage

if [ ! -d "${shared}/data/" ]
then
    echo "ERROR: Shared folder ${shared} does not contain a 'data' folder"
    exit 1
fi

# Delete previous builds from shared storage
echo "Cleaning up old packages"
latest=$(readlink -f "${shared}/data/packages/${pkgset}/.latest")
find "${shared}/data/packages/${pkgset}" -type d -name '.real_*' -maxdepth 1 -not -path "${latest}" -print0 | xargs -0 rm -Rf --

# Copy the latest packages to a local filesystem
sync "${shared}/data/packages/${pkgset}" "/usr/local/poudriere/data/packages/${pkgset}"

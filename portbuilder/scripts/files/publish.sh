#!/bin/sh

usage() {
    echo "USAGE: publish.sh -p <pkgset> -b <s3bucket> -d <s3dir>"
    echo "  pkgset: The unique name of the poudriere package bundle (ex: freebsd-12-2-amd64-daily-freeipa)"
    echo "  s3bucket: The name of the AWS S3 bucket to which the packages should be published"
    echo "  s3dir:  The S3 directory to which the packages should be published"
    exit 1
}

# Parse arguments
while getopts p:b:d: opt
do
  case "$opt" in
    p) pkgset=$OPTARG;;
    b) s3bucket=$OPTARG;;
    d) s3dir=$OPTARG;;
    \?) usage
  esac
done

# Validate arguments
test -n "${pkgset}" || usage
test -n "${s3bucket}" || usage
test -n "${s3dir}" || usage

if [ ! -d "/usr/local/poudriere/data/packages/${pkgset}" ]
then
    echo "ERROR: Package set ${pkgset} not found in /usr/local/poudriere/data/packages"
    exit 1
fi

echo "Generating static index files..."

# Delete existing static index files
find "/usr/local/poudriere/data/packages/${pkgset}" -name index.html -delete

# Generate new static index files
fetch -q "http://localhost/packages/${pkgset}/" -o "/usr/local/poudriere/data/packages/${pkgset}/index.html"
fetch -q "http://localhost/packages/${pkgset}/All/" -o "/usr/local/poudriere/data/packages/${pkgset}/All/index.html"
fetch -q "http://localhost/packages/${pkgset}/Latest/" -o "/usr/local/poudriere/data/packages/${pkgset}/Latest/index.html"

# Change paths in index files
find "/usr/local/poudriere/data/packages/${pkgset}" -name index.html -exec sed -i.bak "s|packages/${pkgset}|${s3dir}|g" {} \;
find "/usr/local/poudriere/data/packages/${pkgset}" -name index.html.bak -delete

echo "Done."

# Set source and target variables
sourcedir="/usr/local/poudriere/data/packages/${pkgset}"
targetdir="s3://${s3bucket}/${s3dir}"

# Copy files to AWS S3
aws s3 cp "${sourcedir}/index.html" "${targetdir}/"
aws s3 cp "${sourcedir}/.latest/meta.conf" "${targetdir}/"
aws s3 cp "${sourcedir}/.latest/meta" "${targetdir}/"

find -L "${sourcedir}/.latest" -name 'packagesite.*' -print0 | xargs -0 -n1 -I '{}' aws s3 cp '{}' "${targetdir}/"
find -L "${sourcedir}/.latest" -name 'data.*' -print0 | xargs -0 -n1 -I '{}' aws s3 cp '{}' "${targetdir}/"

aws s3 sync "${sourcedir}/All/" "${targetdir}/All/" --delete
aws s3 sync "${sourcedir}/Latest/" "${targetdir}/Latest/" --delete --exclude "pkg.txz.sig"

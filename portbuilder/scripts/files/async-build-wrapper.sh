#!/bin/sh
set -e

# Create the heartbeat hook
if [ "$TASK_TOKEN" != "" ]; then
cat > /usr/local/etc/poudriere.d/hooks/pkgbuild.sh << EOF
#!/bin/sh
aws stepfunctions send-task-heartbeat --region ${AWS_SSM_REGION_NAME} --task-token ${TASK_TOKEN}
exit 0
EOF
fi

# Poudriere does not work without sudo
sudo /usr/local/portbuilder/buildports.sh $*

#!/bin/sh

usage() {
    echo "USAGE: push-shared.sh -p <pkgset> -s <shared_folder>"
    echo "  pkgset: The unique name of the poudriere package bundle (ex: freebsd-12-2-amd64-daily-freeipa)"
    echo "  shared: The name shared folder containing the Poudriere 'data' folder"
    exit 1
}

sync() {
  SOURCE=$1
  DEST=$2

  mkdir -p "${SOURCE}"
  mkdir -p "${DEST}"
  echo "Synchronizing from ${SOURCE} to ${DEST}"
  rsync -aHAXEx "${SOURCE}/" "${DEST}"
}

# Parse arguments
while getopts p:s: opt
do
  case "$opt" in
    p) pkgset=$OPTARG;;
    s) shared=$OPTARG;;
    \?) usage
  esac
done

# Validate arguments
test -n "${pkgset}" || usage
test -n "${shared}" || usage

if [ ! -d "${shared}/data/" ]
then
    echo "ERROR: Shared folder ${shared} does not contain a 'data' folder"
    exit 1
fi

if [ ! -d "/usr/local/poudriere/data/packages/${pkgset}" ]
then
    echo "ERROR: Package set ${pkgset} not found in /usr/local/poudriere/data/packages"
    exit 1
fi

sync "/usr/local/poudriere/data/packages/${pkgset}" "${shared}/data/packages/${pkgset}"
sync "/usr/local/poudriere/data/logs/bulk/${pkgset}" "${shared}/data/logs/bulk/${pkgset}"

# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


{{ formula.namespace }}.{{ formula.script.directory }}:
  file.directory:
    - name: {{ formula.script.directory | yaml_encode }}
    - user: {{ formula.script.user | yaml_encode }}
    - group: {{ formula.script.group | yaml_encode }}
    - mode: {{ formula.script.dir_mode | yaml_encode }}


{% for filename in formula.files %}

{{ formula.namespace }}.{{ filename }}:
  file.managed:
    - name: {{ formula.script.directory | path_join(filename) }}
    - source: {{ formula.url ~ "files" | path_join(filename) }}
    - template: jinja
    - user: {{ formula.script.user | yaml_encode }}
    - group: {{ formula.script.group | yaml_encode }}
    - mode: {{ formula.script.file_mode | yaml_encode }}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - file: {{ formula.namespace }}.{{ formula.script.directory }}

{% endfor %}

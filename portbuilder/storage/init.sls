# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{{ formula.namespace }}.zpool:
  zpool.present:
    - name: {{ formula.zpool.name }}
    - config:
        import: true
    - layout:
      - {{ formula.zpool.device }}

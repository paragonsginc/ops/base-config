# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{% for script in ["aws-async-task"] %}

{{ formula.namespace }}.scripts.{{ script }}:
  file.managed:
    - name: {{ formula.scripts.path | path_join(script) | yaml_encode }}
    - source: {{ (formula.scripts.source ~ "/" ~ script ~ ".sh") | yaml_encode }}
    - template: null
    - backup: minion
    - user: {{ formula.scripts.user | yaml_encode }}
    - group: {{ formula.scripts.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.scripts.mode | yaml_encode }}
{% endif %}

{% endfor %}

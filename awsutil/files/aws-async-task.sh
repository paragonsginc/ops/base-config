#!/bin/sh

# Save the async task token
export TASK_TOKEN=$1
shift

# Run the original command
echo Running command: $*
$*
retcode=$?
echo Return code: $retcode

# Report status to AWS
if [ "$retcode" -eq 0 ]
then
    aws stepfunctions send-task-success --region ${AWS_SSM_REGION_NAME} --task-token ${TASK_TOKEN} --task-output "{\"Instance\": {\"Id\": \"${AWS_SSM_INSTANCE_ID}\"}}"
else
    aws stepfunctions send-task-failure --region ${AWS_SSM_REGION_NAME} --task-token ${TASK_TOKEN}
fi

exit $retcode

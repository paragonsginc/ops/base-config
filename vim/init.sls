{% set pkgname = salt['grains.filter_by']({
    'RedHat': 'vim-enhanced',
    'Debian': 'vim',
    'FreeBSD': 'vim',
    'Arch': 'vim'
}, default='Debian') %}

vim:
  pkg.installed:
    - name: '{{ pkgname }}'

########################################################################
#
# WordPress High-Availability Node
#
# Does not include the WordPress installation, because
# that is already present on the EFS share.
########################################################################

include:
  - wordpress.prereqs
  - wordpress.sharedfs
  - wordpress.nginx
  - wordpress.cron

# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


{{ formula.namespace }}.install:
  pip.installed:
    - name: {{ formula.pkgspec | yaml_encode }}

{{ formula.namespace }}.deploy:
  file.managed:
    - name: {{ formula.logger.path | yaml_encode }}
    - source: {{ formula.logger.source | yaml_encode }}
    - template: jinja
    - backup: minion
    - user: {{ formula.logger.user | yaml_encode }}
    - group: {{ formula.logger.group | yaml_encode }}
    - mode: {{ formula.logger.mode | yaml_encode }}
    - require:
      - pip: {{ formula.namespace }}.install

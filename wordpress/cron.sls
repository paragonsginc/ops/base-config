# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


include:
  - {{ formula.namespace }}.prereqs


{{ formula.namespace }}.wp-cron:
  cron.present:
    - identifier: wp-cron
    - name: cd /usr/local/www/wordpress; php -q wp-cron.php >/dev/null 2>&1
    - minute: "*/1"

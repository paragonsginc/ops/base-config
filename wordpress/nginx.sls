# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{%- set formula_path="nginx" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as nginx with context -%}

include:
  - nginx


{{ formula.namespace }}.nginx.config:
  file.managed:
    - name: {{ nginx.configdir.path | path_join(formula.name ~ ".conf") | yaml_encode }}
    - source: {{ formula.nginx.configfile.source | yaml_encode }}
    - template: jinja
    - backup: minion
    - user: {{ formula.nginx.configfile.user | yaml_encode }}
    - group: {{ formula.nginx.configfile.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.nginx.configfile.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - pkg: {{ nginx.namespace }}.install
    - watch_in:
      - service: {{ nginx.namespace }}.start

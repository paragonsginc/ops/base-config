# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


include:
  - nfs.client


{{ formula.namespace }}.sharedfs:

  file.directory:
    - name: {{ formula.sharedfs.path | yaml_encode }}
{% if not salt["file.directory_exists"](formula.sharedfs.path) %}
    - user: {{ formula.sharedfs.user | yaml_encode }}
    - group: {{ formula.sharedfs.group | yaml_encode }}
    - mode: {{ formula.sharedfs.mode | yaml_encode }}
    - makedirs: true
{% endif %}


  mount.mounted:
    - name: {{ formula.sharedfs.path | yaml_encode }}
    - device: {{ formula.sharedfs.device | yaml_encode }}
    - fstype: {{ formula.sharedfs.fstype | yaml_encode }}
    - opts:
{{ formula.sharedfs.opts | yaml(false) | indent(6, true) }}
    - hidden_opts:
{{ formula.sharedfs.opts | yaml(false) | indent(6, true) }}
    - require:
      - file: {{ formula.namespace }}.sharedfs
      - service: nfs.client-service
      - module: nfs.client-service

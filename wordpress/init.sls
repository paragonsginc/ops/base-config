# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


include:
  - {{ formula.namespace }}.prereqs


{{ formula.namespace }}.install:
  pkg.installed:
    - name: {{ formula.package | yaml_encode }}

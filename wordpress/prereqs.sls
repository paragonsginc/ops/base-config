# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{%- set formula_path="php" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as php with context -%}
{% set extprefix = "php" ~ php.version | replace(".", "") ~ "-" -%}

include:
  - {{ php.namespace }}.fpm
  - mariadb.client


{{ formula.namespace }}.extensions:
  pkg.installed:
    - watch_in:
      - service: {{ php.namespace }}.fpm.start
    - pkgs:
{% for extension in formula.php.extensions %}
      - {{ extprefix ~ extension }}
{% endfor %}

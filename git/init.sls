{% set pkgname = salt['grains.filter_by']({
    'RedHat': 'git',
    'Debian': 'git',
    'FreeBSD': 'git',
    'Arch': 'git'
}) %}

git:
  pkg.installed:
    - name: '{{ pkgname }}'


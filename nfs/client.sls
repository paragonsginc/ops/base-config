# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


{{ formula.namespace }}.client-service:

  service.enabled:
    - name: {{ formula.client.service }}

  module.run:
    - service.start:
      - name: {{ formula.client.service }}
    - onchanges:
      - service: {{ formula.namespace }}.client-service

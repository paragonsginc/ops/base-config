# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


include:
  - {{ formula.namespace }}


{{ formula.namespace }}.fpm-www.config:
  file.managed:
    - name: {{ formula.fpm.www.configfile.path | yaml_encode }}
    - source: {{ formula.fpm.www.configfile.source | yaml_encode }}
    - template: jinja
    - backup: minion
    - user: {{ formula.fpm.www.configfile.user | yaml_encode }}
    - group: {{ formula.fpm.www.configfile.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.fpm.www.configfile.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - pkg: {{ formula.namespace }}.install


{{ formula.namespace }}.fpm.start:
  service.running:
    - name: {{ formula.fpm.service | yaml_encode }}
    - enable: true
    - reload: true
    - require:
      - pkg: {{ formula.namespace }}.install
    - watch:
      - file: {{ formula.namespace }}.fpm-www.config

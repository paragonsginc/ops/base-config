# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


{{ formula.namespace }}.install:
  pkg.installed:
    - name: {{ formula.package | yaml_encode }}


{{ formula.namespace }}.config:
  file.managed:
    - name: {{ formula.inifile.path | yaml_encode }}
    - source: {{ formula.inifile.source | yaml_encode }}
    - template: jinja
    - backup: minion
    - user: {{ formula.inifile.user | yaml_encode }}
    - group: {{ formula.inifile.group | yaml_encode }}
{% if grains.os_family != "Windows" %}
    - mode: {{ formula.inifile.mode | yaml_encode }}
{% endif %}
    - context:
        tplfile: {{ tplfile }}
    - require:
      - pkg: {{ formula.namespace }}.install

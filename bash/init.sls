{% set pkgname = salt['grains.filter_by']({
    'RedHat': 'bash',
    'Debian': 'bash',
    'FreeBSD': 'bash',
    'Arch': 'bash'
}) %}

bash:
  pkg.installed:
    - name: '{{ pkgname }}'


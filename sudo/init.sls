sudo-package:
  pkg.installed:
    - name: sudo
    - require:
      - file: sudo-include-dir

sudo-include-dir:
  file.directory:
    - name: /usr/local/etc/sudoers.d

sudo-default-config:
  file.append:
    - name: /usr/local/etc/sudoers
    - require:
      - pkg: sudo-package
    - text:
      - '%wheel ALL=(ALL) NOPASSWD: ALL'
      - Defaults   env_reset
      - Defaults   secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
      - '#includedir /usr/local/etc/sudoers.d'

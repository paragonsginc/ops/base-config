pf_rules:
  sysrc.managed:
    - value: '/etc/pf.conf'

  file.managed:
    - name: '/etc/pf.conf'
    - source: 'salt://pf/files/pf.conf'
    - template: 'jinja'
    - backup: minion

pf_flags:
  sysrc.managed:
    - value: ''

pf:
  service.running:
    - enable: true
    - require:
      - sysrc: 'pf_rules'
      - sysrc: 'pf_flags'
      - file: 'pf_rules'
    - watch:
      - sysrc: 'pf_rules'
      - sysrc: 'pf_flags'
      - file: 'pf_rules'

pflog_logfile:
  sysrc.managed:
    - value: '/var/log/pflog'

pflog_flags:
  sysrc.managed:
    - value: ''

pflog:
  service.running:
    - enable: true
    - require:
      - sysrc: 'pflog_logfile'
      - sysrc: 'pflog_flags'
    - watch:
      - sysrc: 'pflog_logfile'
      - sysrc: 'pflog_flags'

pf_reload:
  cmd.run:
    - name: 'pfctl -vf /etc/pf.conf'
    - onchanges:
      - file: 'pf_rules'

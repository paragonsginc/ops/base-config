lock-root:
  cmd.run:
    - name: pw user mod root -w no
    - unless: pw user show root | grep -q "root:\*:"

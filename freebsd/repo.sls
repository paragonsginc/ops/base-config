freebsd-repo:
  file.managed:
    - name: '/usr/local/etc/pkg/repos/FreeBSD.conf'
    - source: 'salt://freebsd/files/FreeBSD.conf'
    - mode: '0644'
    - user: 'root'
    - group: 'wheel'
    - makedirs: true

pkg-repo-dir:
  file.directory:
    - name: '/usr/local/etc/pkg/repos'
    - mode: '0755'
    - user: 'root'
    - group: 'wheel'
    - clean: false
    - require:
      - file: 'freebsd-repo'

sendmail_enable:
  sysrc.managed:
    - value: 'NO'

sendmail_submit_enable:
  sysrc.managed:
    - value: 'NO'

sendmail_outbound_enable:
  sysrc.managed:
    - value: 'NO'

sendmail_msp_queue_enable:
  sysrc.managed:
    - value: 'NO'

syslogd_flags:
  sysrc.managed:
    - value: -ss

sendmail_service:
  service.dead:
    - enable: false
    - name: sendmail

syslogd:
  service.running:
    - enable: true
    - watch:
      - sysrc: syslogd_flags

login-conf:
  file.managed:
    - name: /etc/login.conf
    - source: salt://freebsd/files/login.conf
    - template: jinja
    - user: root
    - group: wheel
    - mode: '0644'
    - backup: minion

capabilities-database:
  cmd.run:
    - name: cap_mkdb /etc/login.conf
    - onchanges:
      - file: login-conf

{% if salt['file.directory_exists']('/home') and not salt['file.is_link']('/home') %}
home-dir-base:
  file.directory:
    - name: /home
    - user: root
    - group: wheel
    - mode: '0755'

home-dir-link:
  file.symlink:
    - name: /usr/home
    - target: /home
    - user: root
    - group: wheel
    - mode: '0755'

{% else %}

home-dir-base:
  file.directory:
    - name: /usr/home
    - user: root
    - group: wheel
    - mode: '0755'

home-dir-link:
  file.symlink:
    - name: /home
    - target: /usr/home
    - user: root
    - group: wheel
    - mode: '0755'
{% endif %}

root-shell:
  user.present:
    - name: root
    - shell: /bin/sh

pkg.conf:
  file.managed:
    - name: /usr/local/etc/pkg.conf
    - source: salt://freebsd/files/pkg.conf
    - user: root
    - group: wheel
    - mode: '0644'
    - backup: minion

periodic.conf:
  file.managed:
    - name: /etc/periodic.conf
    - source: salt://freebsd/files/periodic.conf
    - mode: '0644'
    - template: jinja

zfs-service:
  service.enabled:
    - name: zfs

  cmd.run:
    - name: service zfs start
    - onchanges:
      - service: zfs-service

dhclient_program:
  sysrc.managed:
    - name: dhclient_program
    - value: /sbin/dhclient

dhcp6-client-enabled:
  sysrc.managed:
    - name: dhcp6c_enable
    - value: 'NO'

{% for ifname, ifprops in salt["network.interfaces"]().items() -%}
{% if ifprops.up and 'inet' in ifprops and not salt["network.is_loopback"](ifprops.inet[0].address) %}

restart-dhcp-{{ ifname }}:
  cmd.run:
    - name: service dhclient restart {{ ifname }}
    - onchanges:
      - sysrc: dhcp6-client-enabled
      - sysrc: dhclient_program

{% endif %}
{% endfor %}

root-vimrc:
  file.managed:
    - name: /root/.vimrc
    - source: salt://freebsd/files/vimrc
    - user: root
    - group: wheel
    - mode: '0644'
    - backup: minion

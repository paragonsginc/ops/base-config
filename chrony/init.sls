ntpd-disable:
  service.dead:
    - name: ntpd
    - enable: false

chrony:

  pkg.installed: []

  service.running:
    - name: chronyd
    - enable: true
    - require:
      - service: ntpd-disable
      - pkg: chrony
      - file: chrony-config
    - watch:
      - file: chrony-config

chrony-config:
  file.managed:
    - name: /usr/local/etc/chrony.conf
    - source: salt://chrony/files/chrony.conf
    - user: root
    - group: wheel
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: chrony

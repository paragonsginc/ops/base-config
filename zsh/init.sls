{% set pkgnames = salt['grains.filter_by']({
    'Debian': ['zsh','zsh-syntax-highlighting'],
    'FreeBSD': ['zsh','zsh-completions','zsh-navigation-tools','zsh-autosuggestions','zsh-syntax-highlighting'],
}) %}


zsh:
  pkg.installed:
    - pkgs:
{% for pkgname in pkgnames %}
      - {{ pkgname }}
{% endfor %}

{% if salt['grains.get']('os_family') == 'FreeBSD' %}

zsh-symlink:
  file.symlink:
    - name: '/bin/zsh'
    - target: '/usr/local/bin/zsh'
    - user: 'root'
    - group: 'wheel'
    - mode: '0755'
    - require:
      - pkg: 'zsh'
    - require_in:
      - file: 'zsh-shell'

zsh-shell:
  file.append:
    - name: '/etc/shells'
    - text: '/bin/zsh'

{% endif %}

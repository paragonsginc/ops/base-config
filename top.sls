base:
  "*":
    - base
{% for role in grains.roles %}
    - role.{{ role }}
{% endfor %}

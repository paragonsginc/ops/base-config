include:
{% if grains.os_family == "FreeBSD" %}
  - freebsd.repo
  - freebsd.core
{% endif %}

  - awsutil
  - bash
  - zsh
  - fish
  - git
  - vim
  - sudo

{% if grains.os_family == "FreeBSD" %}
  - chrony
  - freebsd.utils
{% endif %}

# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}


{{ formula.namespace }}.client.install:
  pkg.installed:
    - name: {{ formula.client.package | yaml_encode }}

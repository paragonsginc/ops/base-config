# vim: sts=2 ts=2 sw=2 et ai ft=jinja #
{% from "blacklistd/map.jinja" import blacklistd with context -%}

include:
  - pf

sshd_flags:
  sysrc.managed:
    - value: '-o UseBlacklist=yes'

blacklistd-sshd:
  service.running:
    - name: sshd
    - enable: true
    - require:
      - sysrc: 'sshd_flags'
    - listen:
      - sysrc: 'sshd_flags'

blacklistd_flags:
  sysrc.managed:
    - value: '-r'

blacklistd-conf:
  file.managed:
    - name: {{ blacklistd.configfile }}
    - source: 'salt://blacklistd/files/blacklistd.conf'
    - template: 'jinja'
    - mode: '0644'

blacklistd:
  service.running:
    - name: {{ blacklistd.service }}
    - enable: true
    - require:
      - sysrc: 'blacklistd_flags'
      - file: 'blacklistd-conf'
    - watch:
      - sysrc: 'blacklistd_flags'
      - file: 'blacklistd-conf'

